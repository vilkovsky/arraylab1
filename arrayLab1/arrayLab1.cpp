﻿#include "pch.h"
#include <iostream>
#include <locale.h>
#include <time.h>
#include "CArrayDeleteFirstZero.h"

using namespace std;

void setOptions() {
	setlocale(LC_ALL, "Russian");
	system("color F0");
}

void menu() {
	CArrayDeleteFirstZero ar;

	int key, n, k, val;
	srand(time(NULL));
	do {
		cout << "1 - создание массива" << endl;
		cout << "2 - изменение элементов массива" << endl;
		cout << "3 - заполнение массива случайными числами" << endl;
		cout << "4 - вывод на экран элементов массива" << endl;
		cout << "5 - удаление из массива первого найденного нулевого элемента со сдвигом элементов" << endl;
		cout << ("Ваш выбор: ");

		cin >> key;

		switch (key)
		{
		case 1:
			cout << ("Введите количество элементов массива: ");
			cin >> n;
			ar.CreateArray(n);
			break;
		case 2:
			cout << ("Введите номер элемента массива: ");
			cin >> k;
			cout << ("Введите значение элемента массива: ");
			cin >> val;
			ar.SetValue(k, val);
			break;
		case 3:
			ar.FillArrayRandValues();
			break;
		case 4:
			ar.DisplayArray();
			break;
		case 5:
			ar.DeleteFirstZero();
			break;
		}


	} while (key != 27);
}

int main()
{
	setOptions();
	menu();

	return 0;
}

