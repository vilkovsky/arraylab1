#pragma once
class CArrayDeleteFirstZero
{
	int nCount;
	int *arr;

public:
	CArrayDeleteFirstZero();
	~CArrayDeleteFirstZero();
	void CreateArray(int N);
	void SetValue(int k, int value);
	void FillArrayRandValues(); 
	void DisplayArray(); 
	void DeleteFirstZero();

};

