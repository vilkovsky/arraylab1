#include "pch.h"
#include "CArrayDeleteFirstZero.h"
#include <iostream>

using namespace std;

CArrayDeleteFirstZero::CArrayDeleteFirstZero()
{
	nCount = 0;
	arr = NULL;

}

CArrayDeleteFirstZero::~CArrayDeleteFirstZero()
{
	delete[] arr;
}

void CArrayDeleteFirstZero::CreateArray(int N) {
	if (N > 0)
	{
		if (arr != NULL)
			delete[] arr;
		arr = new int[N];
		nCount = N;
		cout << "������ ������" << endl;
	}
	else
		cout << "������! ������ ������� ������ ���� ������ 0" << endl;
}

void CArrayDeleteFirstZero::SetValue(int k, int value) {
	if (k >= 0 && k < nCount)
		arr[k] = value;
	else
		cout << "������! ����� �������� ������ �������" << endl;
}

void CArrayDeleteFirstZero::FillArrayRandValues()
{
	if (nCount > 0)
	{
		for (int i = 0; i < nCount; i++)
			arr[i] = rand() % 10; 
		DisplayArray();
	}
	else
		cout << "������ �� ������" << endl;
}

void CArrayDeleteFirstZero::DisplayArray()
{
	if (nCount > 0)
	{
		for (int i = 0; i < nCount; i++)
			cout << arr[i];
		cout << endl;
	}
	else
		cout << "������ �� ������" << endl;
}

void CArrayDeleteFirstZero::DeleteFirstZero() {
	if (nCount > 0) {
		int i;
		for (i = 0; i < nCount; i++) {
			if (arr[i] == 0) {				
				cout << "������ 0 � ������� �� �������: " << i << endl;

				for (int j = i; j < nCount; j++) {
					arr[j] = arr[j + 1];
				}
				
				size_t newSize = nCount - 1;
				int* newArr = new int[newSize];
				memcpy(newArr, arr, nCount * sizeof(int));
				nCount = newSize;
				delete[] arr;
				arr = newArr;

				break;
			}
		}
		if (i == nCount) {
			cout << "0 � ������� �� ������" << endl;
		}
	}
	else
		cout << "������ �� ������" << endl;
}
